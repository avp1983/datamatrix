/**
 * libdmtx - Data Matrix Encoding/Decoding Library
 * Copyright 2007, 2008, 2009 Mike Laughton. All rights reserved.
 *
 * See LICENSE file in the main project directory for full
 * terms of use and distribution.
 *
 * Contact: Mike Laughton <mike@dragonflylogic.com>
 *
 * \file image.c
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <SDL/SDL.h>
#include <SDL/SDL_opengl.h>
//#include <png.h>
#include "dmtx.h"
#include "rotate_test.h"
#include "image.h"
#include "minIni.h"
/**
 *
 *
 */
int
loadTextureImage(int *width, int *height)
{
   unsigned char *pxl;
   pxl = get_code(width, height);
   if (pxl==NULL) return 0;

   /* Set up texture */
   glGenTextures(1, &barcodeTexture);
   glBindTexture(GL_TEXTURE_2D, barcodeTexture);
   glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

   glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
   glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
   glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
   glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

   /* Read barcode image */
   gluBuild2DMipmaps(GL_TEXTURE_2D, 3, *width, *height, GL_RGB, GL_UNSIGNED_BYTE, pxl);

   /* Create the barcode list */
   barcodeList = glGenLists(1);
   glNewList(barcodeList, GL_COMPILE);
   DrawBarCode();
   glEndList();

   return 1;
}


unsigned char*
get_code(int *width0, int *height0){
	size_t          width, height, bytesPerPixel;
    unsigned char  *pxl;
    DmtxEncode     *enc;
    enc = dmtxEncodeCreate();
    enc = setPropFromIni(enc);
    if (enc == NULL){
    	return NULL;
    }
    width = dmtxImageGetProp(enc->image, DmtxPropWidth);
    height = dmtxImageGetProp(enc->image, DmtxPropHeight);
    bytesPerPixel = dmtxImageGetProp(enc->image, DmtxPropBytesPerPixel);
    pxl = (unsigned char *)malloc(width * height * bytesPerPixel);
    assert(pxl != NULL);
    memcpy(pxl, enc->image->pxl, width * height * bytesPerPixel);
    dmtxEncodeDestroy(&enc);
    *width0 = (int)width;
    *height0 = (int)height;
    return pxl;
}

DmtxEncode* setPropFromIni(DmtxEncode   *enc){
	long n;
	char symbol_size[100];

	char *text;
	const char inifile[] = "test.ini";

	/* value reading */
	int module_size = ini_getl("generate_datamatrix", "module_size", -1, inifile);
	assert(module_size!=0);
	int margin_size = ini_getl("generate_datamatrix", "margin_size", -1, inifile);

	/* string reading */
	n = ini_gets("generate_datamatrix", "symbol_size", "dummy", symbol_size, sizearray(symbol_size), inifile);
	assert(n!=0);
	n=ini_gets("generate_datamatrix", "file", "dummy", File_Name, sizearray(File_Name), inifile);
	assert(n!=0);
	int SizeRequest = strToDmtxPropSizeRequest(symbol_size);
	assert(SizeRequest!=0);

	dmtxEncodeSetProp(enc, DmtxPropImageFlip, DmtxFlipY);
	dmtxEncodeSetProp(enc, DmtxPropMarginSize, margin_size);
	dmtxEncodeSetProp(enc, DmtxPropModuleSize, module_size);
	dmtxEncodeSetProp(enc, DmtxPropSizeRequest, SizeRequest);

	text = ReadFile2(File_Name);
	if (text!=NULL){
	    puts(text);
	    dmtxEncodeDataMatrix(enc, strlen(text), text);
	} else {
	    //puts("No text");
	    return NULL;
	}
	return enc;
}



char* ReadFile2(char *filename)
{
	FILE *fin;
	char *buffer = NULL;
	fin = fopen( filename, "r");
	int c, string_size, i;
	i=0;
	if (fin){
		string_size = get_string_size(fin);
		if (string_size==0) return NULL;
		buffer = (char*) malloc (sizeof(char) * string_size );
		while( (c=getc(fin)) != EOF)
		{
			if (i>=string_size) break;
			buffer[i]=c;
			i++;
		}
		buffer[i] = '\0';
		fclose(fin);
		return buffer;
	} else {
		//fprintf(stdout, "File  '%s' did't exists \n", filename);
		return NULL;

	}
}

int get_string_size(FILE *handler){
	 int string_size,read_size;
	 //seek the last byte of the file
	 fseek(handler,0,SEEK_END);
	 //offset from the first to the last byte, or in other words, filesize
	 string_size = ftell (handler);
	 //go back to the start of the file
	 rewind(handler);
     //allocate a string that can hold it all
     return string_size-1;
}

char* ReadFile(char *filename)
{
   char *buffer = NULL;
   int string_size,read_size;
   FILE *handler = fopen(filename,"r");

   if (handler)
   {
       //seek the last byte of the file
       fseek(handler,0,SEEK_END);
       //offset from the first to the last byte, or in other words, filesize
       string_size = ftell (handler);
       //go back to the start of the file
       rewind(handler);

       //allocate a string that can hold it all
       buffer = (char*) malloc (sizeof(char) * (string_size + 1) );
       //read it all in one operation
       read_size = fread(buffer,sizeof(char),string_size,handler);
       //fread doesnt set it so put a \0 in the last position
       //and buffer is now officialy a string
       buffer[string_size+1] = '\0';

       if (string_size != read_size) {
           //something went wrong, throw away the memory and set
           //the buffer to NULL
           free(buffer);
           buffer = NULL;
        }
    } else {
    	fprintf(stdout, "File  '%s' did't exists \n", filename);
    	exit(1);
    }

    return buffer;
}



int strToDmtxPropSizeRequest(char *s){
	int i;

	char *symbolSizes[] = {
	      "10x10", "12x12",   "14x14",   "16x16",   "18x18",   "20x20",
	      "22x22", "24x24",   "26x26",   "32x32",   "36x36",   "40x40",
	      "44x44", "48x48",   "52x52",   "64x64",   "72x72",   "80x80",
	      "88x88", "96x96", "104x104", "120x120", "132x132", "144x144",
	       "8x18",  "8x32",   "12x26",   "12x36",   "16x36",   "16x48"
	};

	 for(i = 0; i < DmtxSymbolSquareCount + DmtxSymbolRectCount; i++) {
		 if(strncmp(s, symbolSizes[i], 8) == 0) {
			 return i;

		 }
	  }

	  return DmtxFail;
}

