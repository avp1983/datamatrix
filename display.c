/**
 * libdmtx - Data Matrix Encoding/Decoding Library
 * Copyright 2007, 2008, 2009 Mike Laughton. All rights reserved.
 *
 * See LICENSE file in the main project directory for full
 * terms of use and distribution.
 *
 * Contact: Mike Laughton <mike@dragonflylogic.com>
 *
 * \file display.c
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <SDL/SDL.h>
#include <SDL/SDL_opengl.h>
#include "dmtx.h"
#include "rotate_test.h"
#include "display.h"


/**
 *
 *
 */
SDL_Surface *initDisplay(void)
{
   SDL_Surface *screen;

   SDL_Init(SDL_INIT_VIDEO);
   info = SDL_GetVideoInfo();
   info->current_w = info->current_w-100;
   info->current_h = info->current_h-100;
   screen = SDL_SetVideoMode(info->current_w, info->current_h, 16, SDL_OPENGL | SDL_RESIZABLE);
   if(!screen) {
      fprintf(stderr, "Couldn't set 968x646 GL video mode: %s\n", SDL_GetError());
      SDL_Quit();
      exit(2);
   }
   SDL_WM_SetCaption("GL Test", "GL Test");

   glClearColor(0.0, 0.0, 0.3, 1.0);

   return screen;
}

/**
 *
 *
 */
void DrawBarCode(void)
{
   glColor3f(0.95, 0.95, 0.95);
   glBegin(GL_QUADS);
   glTexCoord2d(0.0, 0.0); glVertex3f(-2.0, -2.0,  0.0);
   glTexCoord2d(1.0, 0.0); glVertex3f( 2.0, -2.0,  0.0);
   glTexCoord2d(1.0, 1.0); glVertex3f( 2.0,  2.0,  0.0);
   glTexCoord2d(0.0, 1.0); glVertex3f(-2.0,  2.0,  0.0);
   glEnd();
}

/**
 *
 *
 */
void ReshapeWindow( SDL_Surface *screen, int new_width, int new_height,  int img_width, int img_height)
{
	info->current_w = new_width;
	info->current_h = new_height;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    DrawGeneratedImage(img_width, img_height);
	SDL_GL_SwapBuffers();
}


/**
 *
 *
 */
void DrawGeneratedImage(int width, int height)
{
    int x, y;
	if (width < info->current_w && height < info->current_h){
	   x=(info->current_w - width)/2;
	   y=(info->current_h - height)/2;
   } else {
	   x=2;
	   y=2;
   }

	/* rotate barcode surface */
   glViewport(x, y, (GLint)width, (GLint)height);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   glFrustum(-1.0, 1.0, -1.0, 1.0, 5.0, 50.0);
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
   glTranslatef(0.0, 0.0, -10.0);
   glPolygonMode(GL_FRONT, GL_FILL);
   glPolygonMode(GL_BACK, GL_LINE);
   glEnable(GL_TEXTURE_2D);

   glPushMatrix();
   glRotatef(view_rotx, 1.0, 0.0, 0.0);
   glRotatef(view_roty, 0.0, 1.0, 0.0);
   glRotatef(view_rotz, 0.0, 0.0, 1.0);
   glRotatef(angle, 0.0, 0.0, 1.0);
   glCallList(barcodeList);
   glPopMatrix();
}



/**
 *
 *
 */
int HandleEvent(SDL_Event *event, SDL_Surface *screen,  int *file_deleted,  int img_width, int img_height)
{
  // int width, height;
   //fprintf(stdout, "File  '%d'  \n", *file_deleted);
   switch(event->type) {
      case SDL_VIDEORESIZE:
         screen = SDL_SetVideoMode(event->resize.w, event->resize.h, 16,               SDL_OPENGL | SDL_RESIZABLE);
         if(screen) {
            ReshapeWindow(screen, screen->w, screen->h,  img_width, img_height);
         }
         else {
            return(1);
         }
         break;

      case SDL_QUIT:
         return(1);
         break;

      case SDL_MOUSEMOTION:
         //view_rotx = ((event->motion.y-160)/2.0);
         //view_roty = ((event->motion.x-160)/2.0);
         break;

      case SDL_KEYDOWN:
         switch(event->key.keysym.sym) {
            case SDLK_ESCAPE:
               return(1);
               break;
            case SDLK_SPACE:
            	puts("Space\n");
            	if(remove(File_Name) == -1) {
            		puts("Can not delete file");
            		exit(1);
            	}
            	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            	SDL_GL_SwapBuffers();
            	*file_deleted=1;
            	break;
            default:
               break;
         }
         break;

      case SDL_MOUSEBUTTONDOWN:
         switch(event->button.button) {
            case SDL_BUTTON_RIGHT:
            	fprintf(stdout, "right click\n");
               //free(texturePxl);
               //texturePxl = (unsigned char *)loadTextureImage(&width, &height);
               break;
            case SDL_BUTTON_LEFT:
               fprintf(stdout, "left click\n");
               break;
            default:
               break;
         }
         break;
   }

   return(0);
}
