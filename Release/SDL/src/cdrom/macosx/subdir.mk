################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../SDL/src/cdrom/macosx/AudioFilePlayer.c \
../SDL/src/cdrom/macosx/AudioFileReaderThread.c \
../SDL/src/cdrom/macosx/CDPlayer.c \
../SDL/src/cdrom/macosx/SDLOSXCAGuard.c \
../SDL/src/cdrom/macosx/SDL_syscdrom.c 

OBJS += \
./SDL/src/cdrom/macosx/AudioFilePlayer.o \
./SDL/src/cdrom/macosx/AudioFileReaderThread.o \
./SDL/src/cdrom/macosx/CDPlayer.o \
./SDL/src/cdrom/macosx/SDLOSXCAGuard.o \
./SDL/src/cdrom/macosx/SDL_syscdrom.o 

C_DEPS += \
./SDL/src/cdrom/macosx/AudioFilePlayer.d \
./SDL/src/cdrom/macosx/AudioFileReaderThread.d \
./SDL/src/cdrom/macosx/CDPlayer.d \
./SDL/src/cdrom/macosx/SDLOSXCAGuard.d \
./SDL/src/cdrom/macosx/SDL_syscdrom.d 


# Each subdirectory must supply rules for building sources it contributes
SDL/src/cdrom/macosx/%.o: ../SDL/src/cdrom/macosx/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -I/home/alex/lib_test/include -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


