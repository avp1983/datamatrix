################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../SDL/src/video/ataricommon/SDL_ataridevmouse.c \
../SDL/src/video/ataricommon/SDL_atarievents.c \
../SDL/src/video/ataricommon/SDL_atarigl.c \
../SDL/src/video/ataricommon/SDL_atarimxalloc.c \
../SDL/src/video/ataricommon/SDL_biosevents.c \
../SDL/src/video/ataricommon/SDL_gemdosevents.c \
../SDL/src/video/ataricommon/SDL_ikbdevents.c \
../SDL/src/video/ataricommon/SDL_xbiosevents.c 

S_UPPER_SRCS += \
../SDL/src/video/ataricommon/SDL_ataric2p.S \
../SDL/src/video/ataricommon/SDL_atarieddi.S \
../SDL/src/video/ataricommon/SDL_ikbdinterrupt.S \
../SDL/src/video/ataricommon/SDL_xbiosinterrupt.S 

OBJS += \
./SDL/src/video/ataricommon/SDL_ataric2p.o \
./SDL/src/video/ataricommon/SDL_ataridevmouse.o \
./SDL/src/video/ataricommon/SDL_atarieddi.o \
./SDL/src/video/ataricommon/SDL_atarievents.o \
./SDL/src/video/ataricommon/SDL_atarigl.o \
./SDL/src/video/ataricommon/SDL_atarimxalloc.o \
./SDL/src/video/ataricommon/SDL_biosevents.o \
./SDL/src/video/ataricommon/SDL_gemdosevents.o \
./SDL/src/video/ataricommon/SDL_ikbdevents.o \
./SDL/src/video/ataricommon/SDL_ikbdinterrupt.o \
./SDL/src/video/ataricommon/SDL_xbiosevents.o \
./SDL/src/video/ataricommon/SDL_xbiosinterrupt.o 

C_DEPS += \
./SDL/src/video/ataricommon/SDL_ataridevmouse.d \
./SDL/src/video/ataricommon/SDL_atarievents.d \
./SDL/src/video/ataricommon/SDL_atarigl.d \
./SDL/src/video/ataricommon/SDL_atarimxalloc.d \
./SDL/src/video/ataricommon/SDL_biosevents.d \
./SDL/src/video/ataricommon/SDL_gemdosevents.d \
./SDL/src/video/ataricommon/SDL_ikbdevents.d \
./SDL/src/video/ataricommon/SDL_xbiosevents.d 


# Each subdirectory must supply rules for building sources it contributes
SDL/src/video/ataricommon/%.o: ../SDL/src/video/ataricommon/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Assembler'
	as  -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

SDL/src/video/ataricommon/%.o: ../SDL/src/video/ataricommon/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -I/home/alex/lib_test/include -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


