################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../SDL/src/video/SDL_RLEaccel.c \
../SDL/src/video/SDL_blit.c \
../SDL/src/video/SDL_blit_0.c \
../SDL/src/video/SDL_blit_1.c \
../SDL/src/video/SDL_blit_A.c \
../SDL/src/video/SDL_blit_N.c \
../SDL/src/video/SDL_bmp.c \
../SDL/src/video/SDL_cursor.c \
../SDL/src/video/SDL_gamma.c \
../SDL/src/video/SDL_pixels.c \
../SDL/src/video/SDL_stretch.c \
../SDL/src/video/SDL_surface.c \
../SDL/src/video/SDL_video.c \
../SDL/src/video/SDL_yuv.c \
../SDL/src/video/SDL_yuv_mmx.c \
../SDL/src/video/SDL_yuv_sw.c 

OBJS += \
./SDL/src/video/SDL_RLEaccel.o \
./SDL/src/video/SDL_blit.o \
./SDL/src/video/SDL_blit_0.o \
./SDL/src/video/SDL_blit_1.o \
./SDL/src/video/SDL_blit_A.o \
./SDL/src/video/SDL_blit_N.o \
./SDL/src/video/SDL_bmp.o \
./SDL/src/video/SDL_cursor.o \
./SDL/src/video/SDL_gamma.o \
./SDL/src/video/SDL_pixels.o \
./SDL/src/video/SDL_stretch.o \
./SDL/src/video/SDL_surface.o \
./SDL/src/video/SDL_video.o \
./SDL/src/video/SDL_yuv.o \
./SDL/src/video/SDL_yuv_mmx.o \
./SDL/src/video/SDL_yuv_sw.o 

C_DEPS += \
./SDL/src/video/SDL_RLEaccel.d \
./SDL/src/video/SDL_blit.d \
./SDL/src/video/SDL_blit_0.d \
./SDL/src/video/SDL_blit_1.d \
./SDL/src/video/SDL_blit_A.d \
./SDL/src/video/SDL_blit_N.d \
./SDL/src/video/SDL_bmp.d \
./SDL/src/video/SDL_cursor.d \
./SDL/src/video/SDL_gamma.d \
./SDL/src/video/SDL_pixels.d \
./SDL/src/video/SDL_stretch.d \
./SDL/src/video/SDL_surface.d \
./SDL/src/video/SDL_video.d \
./SDL/src/video/SDL_yuv.d \
./SDL/src/video/SDL_yuv_mmx.d \
./SDL/src/video/SDL_yuv_sw.d 


# Each subdirectory must supply rules for building sources it contributes
SDL/src/video/%.o: ../SDL/src/video/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -I/home/alex/lib_test/include -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


