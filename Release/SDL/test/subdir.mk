################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../SDL/test/checkkeys.c \
../SDL/test/graywin.c \
../SDL/test/loopwave.c \
../SDL/test/testalpha.c \
../SDL/test/testbitmap.c \
../SDL/test/testblitspeed.c \
../SDL/test/testcdrom.c \
../SDL/test/testcursor.c \
../SDL/test/testdyngl.c \
../SDL/test/testerror.c \
../SDL/test/testfile.c \
../SDL/test/testgamma.c \
../SDL/test/testgl.c \
../SDL/test/testhread.c \
../SDL/test/testiconv.c \
../SDL/test/testjoystick.c \
../SDL/test/testkeys.c \
../SDL/test/testloadso.c \
../SDL/test/testlock.c \
../SDL/test/testoverlay.c \
../SDL/test/testoverlay2.c \
../SDL/test/testpalette.c \
../SDL/test/testplatform.c \
../SDL/test/testsem.c \
../SDL/test/testsprite.c \
../SDL/test/testtimer.c \
../SDL/test/testver.c \
../SDL/test/testvidinfo.c \
../SDL/test/testwin.c \
../SDL/test/testwm.c \
../SDL/test/threadwin.c \
../SDL/test/torturethread.c 

OBJS += \
./SDL/test/checkkeys.o \
./SDL/test/graywin.o \
./SDL/test/loopwave.o \
./SDL/test/testalpha.o \
./SDL/test/testbitmap.o \
./SDL/test/testblitspeed.o \
./SDL/test/testcdrom.o \
./SDL/test/testcursor.o \
./SDL/test/testdyngl.o \
./SDL/test/testerror.o \
./SDL/test/testfile.o \
./SDL/test/testgamma.o \
./SDL/test/testgl.o \
./SDL/test/testhread.o \
./SDL/test/testiconv.o \
./SDL/test/testjoystick.o \
./SDL/test/testkeys.o \
./SDL/test/testloadso.o \
./SDL/test/testlock.o \
./SDL/test/testoverlay.o \
./SDL/test/testoverlay2.o \
./SDL/test/testpalette.o \
./SDL/test/testplatform.o \
./SDL/test/testsem.o \
./SDL/test/testsprite.o \
./SDL/test/testtimer.o \
./SDL/test/testver.o \
./SDL/test/testvidinfo.o \
./SDL/test/testwin.o \
./SDL/test/testwm.o \
./SDL/test/threadwin.o \
./SDL/test/torturethread.o 

C_DEPS += \
./SDL/test/checkkeys.d \
./SDL/test/graywin.d \
./SDL/test/loopwave.d \
./SDL/test/testalpha.d \
./SDL/test/testbitmap.d \
./SDL/test/testblitspeed.d \
./SDL/test/testcdrom.d \
./SDL/test/testcursor.d \
./SDL/test/testdyngl.d \
./SDL/test/testerror.d \
./SDL/test/testfile.d \
./SDL/test/testgamma.d \
./SDL/test/testgl.d \
./SDL/test/testhread.d \
./SDL/test/testiconv.d \
./SDL/test/testjoystick.d \
./SDL/test/testkeys.d \
./SDL/test/testloadso.d \
./SDL/test/testlock.d \
./SDL/test/testoverlay.d \
./SDL/test/testoverlay2.d \
./SDL/test/testpalette.d \
./SDL/test/testplatform.d \
./SDL/test/testsem.d \
./SDL/test/testsprite.d \
./SDL/test/testtimer.d \
./SDL/test/testver.d \
./SDL/test/testvidinfo.d \
./SDL/test/testwin.d \
./SDL/test/testwm.d \
./SDL/test/threadwin.d \
./SDL/test/torturethread.d 


# Each subdirectory must supply rules for building sources it contributes
SDL/test/%.o: ../SDL/test/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -I/home/alex/lib_test/include -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


