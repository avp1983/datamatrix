################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../libdmtx/libdmtx_la-dmtx.o 

C_SRCS += \
../libdmtx/dmtx.c \
../libdmtx/dmtxbytelist.c \
../libdmtx/dmtxdecode.c \
../libdmtx/dmtxdecodescheme.c \
../libdmtx/dmtxencode.c \
../libdmtx/dmtxencodeascii.c \
../libdmtx/dmtxencodebase256.c \
../libdmtx/dmtxencodec40textx12.c \
../libdmtx/dmtxencodeedifact.c \
../libdmtx/dmtxencodeoptimize.c \
../libdmtx/dmtxencodescheme.c \
../libdmtx/dmtxencodestream.c \
../libdmtx/dmtximage.c \
../libdmtx/dmtxmatrix3.c \
../libdmtx/dmtxmessage.c \
../libdmtx/dmtxplacemod.c \
../libdmtx/dmtxreedsol.c \
../libdmtx/dmtxregion.c \
../libdmtx/dmtxscangrid.c \
../libdmtx/dmtxsymbol.c \
../libdmtx/dmtxtime.c \
../libdmtx/dmtxvector2.c 

OBJS += \
./libdmtx/dmtx.o \
./libdmtx/dmtxbytelist.o \
./libdmtx/dmtxdecode.o \
./libdmtx/dmtxdecodescheme.o \
./libdmtx/dmtxencode.o \
./libdmtx/dmtxencodeascii.o \
./libdmtx/dmtxencodebase256.o \
./libdmtx/dmtxencodec40textx12.o \
./libdmtx/dmtxencodeedifact.o \
./libdmtx/dmtxencodeoptimize.o \
./libdmtx/dmtxencodescheme.o \
./libdmtx/dmtxencodestream.o \
./libdmtx/dmtximage.o \
./libdmtx/dmtxmatrix3.o \
./libdmtx/dmtxmessage.o \
./libdmtx/dmtxplacemod.o \
./libdmtx/dmtxreedsol.o \
./libdmtx/dmtxregion.o \
./libdmtx/dmtxscangrid.o \
./libdmtx/dmtxsymbol.o \
./libdmtx/dmtxtime.o \
./libdmtx/dmtxvector2.o 

C_DEPS += \
./libdmtx/dmtx.d \
./libdmtx/dmtxbytelist.d \
./libdmtx/dmtxdecode.d \
./libdmtx/dmtxdecodescheme.d \
./libdmtx/dmtxencode.d \
./libdmtx/dmtxencodeascii.d \
./libdmtx/dmtxencodebase256.d \
./libdmtx/dmtxencodec40textx12.d \
./libdmtx/dmtxencodeedifact.d \
./libdmtx/dmtxencodeoptimize.d \
./libdmtx/dmtxencodescheme.d \
./libdmtx/dmtxencodestream.d \
./libdmtx/dmtximage.d \
./libdmtx/dmtxmatrix3.d \
./libdmtx/dmtxmessage.d \
./libdmtx/dmtxplacemod.d \
./libdmtx/dmtxreedsol.d \
./libdmtx/dmtxregion.d \
./libdmtx/dmtxscangrid.d \
./libdmtx/dmtxsymbol.d \
./libdmtx/dmtxtime.d \
./libdmtx/dmtxvector2.d 


# Each subdirectory must supply rules for building sources it contributes
libdmtx/%.o: ../libdmtx/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -I/home/alex/lib_test/include -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


