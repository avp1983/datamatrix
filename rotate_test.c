/**
 * libdmtx - Data Matrix Encoding/Decoding Library
 * Copyright 2007, 2008, 2009 Mike Laughton. All rights reserved.
 *
 * See LICENSE file in the main project directory for full
 * terms of use and distribution.
 *
 * Contact: Mike Laughton <mike@dragonflylogic.com>
 *
 * \file rotate_test.c
 */

#include "rotate_test.h"

#define MIN(x,y) ((x < y) ? x : y)
#define MAX(x,y) ((x > y) ? x : y)

GLfloat view_rotx = 0.0, view_roty = 0.0, view_rotz = 0.0;
GLfloat angle = 0.0;

GLuint barcodeTexture;
GLint barcodeList;

/**
 *
 *
 */
int main(int argc, char *argv[])
{
   int             done, r;
   int             width, height;
   SDL_Event       event;
   SDL_Surface     *screen;
   int file_deleted = 0;
   int file_check_delay_ms = 2000;
   int file_check_timer=2000;
   /* Initialize display window */
   screen = initDisplay();

   /* Load input image to DmtxImage */
   r = loadTextureImage(&width, &height);
   if (r){
	   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	   DrawGeneratedImage(width, height);
	   SDL_GL_SwapBuffers();
   } else {
	   file_deleted = 1;
   }
   done = 0;
   while(!done) {
      SDL_Delay(50);
      file_check_timer+=50;
      if (file_check_timer>=file_check_delay_ms&&file_deleted){
    	  file_check_timer=0;
		  r = loadTextureImage(&width, &height);
		  if (r){
			  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			  DrawGeneratedImage(width, height);
			  SDL_GL_SwapBuffers();
			  file_deleted=0;
		  }
      }
      while(SDL_PollEvent(&event)){
    	  done = HandleEvent(&event, screen, &file_deleted, width, height);
      }
   }
exit(0);
}
