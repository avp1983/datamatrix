################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../libdmtx/test/multi_test/dmtx.c \
../libdmtx/test/multi_test/dmtxaccel.c \
../libdmtx/test/multi_test/dmtxdecode2.c \
../libdmtx/test/multi_test/dmtxhough.c \
../libdmtx/test/multi_test/dmtxregion2.c \
../libdmtx/test/multi_test/dmtxsobel.c \
../libdmtx/test/multi_test/dmtxvaluegrid.c \
../libdmtx/test/multi_test/kiss_fft.c \
../libdmtx/test/multi_test/kiss_fftr.c \
../libdmtx/test/multi_test/multi_test.c \
../libdmtx/test/multi_test/visualize.c 

OBJS += \
./libdmtx/test/multi_test/dmtx.o \
./libdmtx/test/multi_test/dmtxaccel.o \
./libdmtx/test/multi_test/dmtxdecode2.o \
./libdmtx/test/multi_test/dmtxhough.o \
./libdmtx/test/multi_test/dmtxregion2.o \
./libdmtx/test/multi_test/dmtxsobel.o \
./libdmtx/test/multi_test/dmtxvaluegrid.o \
./libdmtx/test/multi_test/kiss_fft.o \
./libdmtx/test/multi_test/kiss_fftr.o \
./libdmtx/test/multi_test/multi_test.o \
./libdmtx/test/multi_test/visualize.o 

C_DEPS += \
./libdmtx/test/multi_test/dmtx.d \
./libdmtx/test/multi_test/dmtxaccel.d \
./libdmtx/test/multi_test/dmtxdecode2.d \
./libdmtx/test/multi_test/dmtxhough.d \
./libdmtx/test/multi_test/dmtxregion2.d \
./libdmtx/test/multi_test/dmtxsobel.d \
./libdmtx/test/multi_test/dmtxvaluegrid.d \
./libdmtx/test/multi_test/kiss_fft.d \
./libdmtx/test/multi_test/kiss_fftr.d \
./libdmtx/test/multi_test/multi_test.d \
./libdmtx/test/multi_test/visualize.d 


# Each subdirectory must supply rules for building sources it contributes
libdmtx/test/multi_test/%.o: ../libdmtx/test/multi_test/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -I/home/alex/lib_test/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


