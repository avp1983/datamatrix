################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../SDL/src/audio/mint/SDL_mintaudio.c \
../SDL/src/audio/mint/SDL_mintaudio_dma8.c \
../SDL/src/audio/mint/SDL_mintaudio_gsxb.c \
../SDL/src/audio/mint/SDL_mintaudio_mcsn.c \
../SDL/src/audio/mint/SDL_mintaudio_stfa.c \
../SDL/src/audio/mint/SDL_mintaudio_xbios.c 

S_UPPER_SRCS += \
../SDL/src/audio/mint/SDL_mintaudio_it.S 

OBJS += \
./SDL/src/audio/mint/SDL_mintaudio.o \
./SDL/src/audio/mint/SDL_mintaudio_dma8.o \
./SDL/src/audio/mint/SDL_mintaudio_gsxb.o \
./SDL/src/audio/mint/SDL_mintaudio_it.o \
./SDL/src/audio/mint/SDL_mintaudio_mcsn.o \
./SDL/src/audio/mint/SDL_mintaudio_stfa.o \
./SDL/src/audio/mint/SDL_mintaudio_xbios.o 

C_DEPS += \
./SDL/src/audio/mint/SDL_mintaudio.d \
./SDL/src/audio/mint/SDL_mintaudio_dma8.d \
./SDL/src/audio/mint/SDL_mintaudio_gsxb.d \
./SDL/src/audio/mint/SDL_mintaudio_mcsn.d \
./SDL/src/audio/mint/SDL_mintaudio_stfa.d \
./SDL/src/audio/mint/SDL_mintaudio_xbios.d 


# Each subdirectory must supply rules for building sources it contributes
SDL/src/audio/mint/%.o: ../SDL/src/audio/mint/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -I/home/alex/lib_test/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

SDL/src/audio/mint/%.o: ../SDL/src/audio/mint/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Assembler'
	as  -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


