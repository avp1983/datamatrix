################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../SDL/src/audio/SDL_audio.c \
../SDL/src/audio/SDL_audiocvt.c \
../SDL/src/audio/SDL_audiodev.c \
../SDL/src/audio/SDL_mixer.c \
../SDL/src/audio/SDL_mixer_MMX.c \
../SDL/src/audio/SDL_mixer_MMX_VC.c \
../SDL/src/audio/SDL_mixer_m68k.c \
../SDL/src/audio/SDL_wave.c 

OBJS += \
./SDL/src/audio/SDL_audio.o \
./SDL/src/audio/SDL_audiocvt.o \
./SDL/src/audio/SDL_audiodev.o \
./SDL/src/audio/SDL_mixer.o \
./SDL/src/audio/SDL_mixer_MMX.o \
./SDL/src/audio/SDL_mixer_MMX_VC.o \
./SDL/src/audio/SDL_mixer_m68k.o \
./SDL/src/audio/SDL_wave.o 

C_DEPS += \
./SDL/src/audio/SDL_audio.d \
./SDL/src/audio/SDL_audiocvt.d \
./SDL/src/audio/SDL_audiodev.d \
./SDL/src/audio/SDL_mixer.d \
./SDL/src/audio/SDL_mixer_MMX.d \
./SDL/src/audio/SDL_mixer_MMX_VC.d \
./SDL/src/audio/SDL_mixer_m68k.d \
./SDL/src/audio/SDL_wave.d 


# Each subdirectory must supply rules for building sources it contributes
SDL/src/audio/%.o: ../SDL/src/audio/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -I/home/alex/lib_test/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


