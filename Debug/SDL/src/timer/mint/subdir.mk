################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../SDL/src/timer/mint/SDL_systimer.c 

S_UPPER_SRCS += \
../SDL/src/timer/mint/SDL_vbltimer.S 

OBJS += \
./SDL/src/timer/mint/SDL_systimer.o \
./SDL/src/timer/mint/SDL_vbltimer.o 

C_DEPS += \
./SDL/src/timer/mint/SDL_systimer.d 


# Each subdirectory must supply rules for building sources it contributes
SDL/src/timer/mint/%.o: ../SDL/src/timer/mint/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -I/home/alex/lib_test/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

SDL/src/timer/mint/%.o: ../SDL/src/timer/mint/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Assembler'
	as  -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


