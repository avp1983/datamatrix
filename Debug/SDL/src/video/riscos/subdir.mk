################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../SDL/src/video/riscos/SDL_riscosFullScreenVideo.c \
../SDL/src/video/riscos/SDL_riscosevents.c \
../SDL/src/video/riscos/SDL_riscosmouse.c \
../SDL/src/video/riscos/SDL_riscossprite.c \
../SDL/src/video/riscos/SDL_riscostask.c \
../SDL/src/video/riscos/SDL_riscosvideo.c \
../SDL/src/video/riscos/SDL_wimppoll.c \
../SDL/src/video/riscos/SDL_wimpvideo.c 

S_UPPER_SRCS += \
../SDL/src/video/riscos/SDL_riscosASM.S 

OBJS += \
./SDL/src/video/riscos/SDL_riscosASM.o \
./SDL/src/video/riscos/SDL_riscosFullScreenVideo.o \
./SDL/src/video/riscos/SDL_riscosevents.o \
./SDL/src/video/riscos/SDL_riscosmouse.o \
./SDL/src/video/riscos/SDL_riscossprite.o \
./SDL/src/video/riscos/SDL_riscostask.o \
./SDL/src/video/riscos/SDL_riscosvideo.o \
./SDL/src/video/riscos/SDL_wimppoll.o \
./SDL/src/video/riscos/SDL_wimpvideo.o 

C_DEPS += \
./SDL/src/video/riscos/SDL_riscosFullScreenVideo.d \
./SDL/src/video/riscos/SDL_riscosevents.d \
./SDL/src/video/riscos/SDL_riscosmouse.d \
./SDL/src/video/riscos/SDL_riscossprite.d \
./SDL/src/video/riscos/SDL_riscostask.d \
./SDL/src/video/riscos/SDL_riscosvideo.d \
./SDL/src/video/riscos/SDL_wimppoll.d \
./SDL/src/video/riscos/SDL_wimpvideo.d 


# Each subdirectory must supply rules for building sources it contributes
SDL/src/video/riscos/%.o: ../SDL/src/video/riscos/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Assembler'
	as  -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

SDL/src/video/riscos/%.o: ../SDL/src/video/riscos/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -I/home/alex/lib_test/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


