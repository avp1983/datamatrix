/**
 * libdmtx - Data Matrix Encoding/Decoding Library
 * Copyright 2007, 2008, 2009 Mike Laughton. All rights reserved.
 *
 * See LICENSE file in the main project directory for full
 * terms of use and distribution.
 *
 * Contact: Mike Laughton <mike@dragonflylogic.com>
 *
 * \file display.h
 */

#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL/SDL.h>

GLfloat view_rotx, view_roty, view_rotz;
GLfloat angle;

GLuint   barcodeTexture;
GLint    barcodeList;

SDL_Surface *initDisplay(void);
void DrawBarCode(void);
void ReshapeWindow( SDL_Surface *screen, int new_width, int new_height, int img_width, int img_height);
void DrawGeneratedImage(int width, int height);
int HandleEvent(SDL_Event *event, SDL_Surface *screen, int *file_deleted,  int img_width, int img_height);

