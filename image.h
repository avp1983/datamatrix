/**
 * libdmtx - Data Matrix Encoding/Decoding Library
 * Copyright 2007, 2008, 2009 Mike Laughton. All rights reserved.
 *
 * See LICENSE file in the main project directory for full
 * terms of use and distribution.
 *
 * Contact: Mike Laughton <mike@dragonflylogic.com>
 *
 * \file image.h
 */

#ifndef __IMAGE_H__
#define __IMAGE_H__

#define IMAGE_NO_ERROR  0
#define IMAGE_ERROR     1
#define IMAGE_NOT_PNG   2
#define sizearray(a)  (sizeof(a) / sizeof((a)[0]))

typedef enum {
   ColorWhite  = 0x01 << 0,
   ColorRed    = 0x01 << 1,
   ColorGreen  = 0x01 << 2,
   ColorBlue   = 0x01 << 3,
   ColorYellow = 0x01 << 4
} ColorEnum;

/*void captureImage(DmtxImage *img, DmtxImage *imgTmp);*/
int loadTextureImage(int *width, int *height);
unsigned char *get_code(int *width0, int *height0);
DmtxEncode* setPropFromIni(DmtxEncode   *enc);
int strToDmtxPropSizeRequest(char *s);

char* ReadFile(char *filename);
char* ReadFile2(char *filename);
int get_string_size(FILE *handler);
#endif
