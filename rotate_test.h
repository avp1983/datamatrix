/**
 * libdmtx - Data Matrix Encoding/Decoding Library
 * Copyright 2007, 2008, 2009 Mike Laughton. All rights reserved.
 *
 * See LICENSE file in the main project directory for full
 * terms of use and distribution.
 *
 * Contact: Mike Laughton <mike@dragonflylogic.com>
 *
 * \file rotate_test.h
 */

#ifndef __SCANDEMO_H__
#define __SCANDEMO_H__

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <SDL/SDL.h>
#include <SDL/SDL_opengl.h>
#include "dmtx.h"
#include "image.h"
#include "display.h"


#define CALLBACK_POINT_PLOT(a,b,c,d) PlotPointCallback(a,b,c,d)
#define CALLBACK_POINT_XFRM(a,b,c,d) XfrmPlotPointCallback(a,b,c,d)
#define CALLBACK_MODULE(a,b,c,d,e)   PlotModuleCallback(a,b,c,d,e)
#define CALLBACK_MATRIX(a)           BuildMatrixCallback2(a)
#define CALLBACK_FINAL(a,b)          FinalCallback(a,b)
#define max(X,Y) (X > Y) ? X : Y
#define min(X,Y) (X < Y) ? X : Y

extern GLfloat       view_rotx;
extern GLfloat       view_roty;
extern GLfloat       view_rotz;
extern GLfloat       angle;

extern GLuint        barcodeTexture;
extern GLint         barcodeList;
SDL_VideoInfo* info;
char File_Name[255];
extern unsigned char *texturePxl;




#endif
